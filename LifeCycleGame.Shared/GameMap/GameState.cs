﻿using LifeCycleGame.Shared.Pieces;
using System.Collections.Generic;

namespace LifeCycleGame.GameStateMap
{
    /// <summary>
    /// This class represents a snapshot/copy of the GameStateMap information. It will be used by both save games and AI players to represent 
    /// a snapshot of the game so they can work on/process, without actually interrupting the real game state (which may be in flux). This should reduce the amount of locking necessary
    /// </summary>
    public class GameState
	{
		public List<GamePiece> Pieces { get; set; }

		public int XDimension { get; set; }

		public int YDimension { get; set; }

		public GameState ()
		{
		}

		public GameState DeepClone ()
		{
			return new GameState () {
				XDimension = this.XDimension,
				YDimension = this.YDimension,
				Pieces = this.Pieces.ConvertAll (p => p.DeepClone ())
			};
		}

	}
}

