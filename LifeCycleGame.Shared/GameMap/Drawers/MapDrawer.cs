﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Graphics;
using LifeCycleGame.Shared.Pieces;

namespace LifeCycleGame.GameStateMap.Drawers
{
    public class MapDrawer
	{
		readonly int _borderPx;
        readonly int _hitRadius;
        public ScreenCoordinate[,] ScreenLocations;
		int _screenXPx, _screenYPx;


		public int XDimension { get; set; }

		public int YDimension { get; set; }

		private GameMap _gameMap;

		public MapDrawer (Context context, int xDimension, int yDimension, GameMap gameMap)
		{
			_gameMap = gameMap;
			XDimension = xDimension;
			YDimension = yDimension;
			_screenXPx = context.Resources.DisplayMetrics.WidthPixels;
			_screenYPx = _screenXPx;
            _borderPx = (int)(_screenXPx / (2 * xDimension)) + 30;
            _hitRadius = (int)(_screenXPx / (2 * xDimension));
            ScreenLocations = new ScreenCoordinate[XDimension + 1, YDimension + 1];
			for (int x = 0; x <= XDimension; x++) {
				for (int y = 0; y <= YDimension; y++) {
					ScreenLocations [x, y] = new ScreenCoordinate (_borderPx + x * ((_screenXPx - (2 * _borderPx)) / XDimension), _borderPx + y * ((_screenYPx - (2 * _borderPx)) / YDimension));
				}
			}
		}

		public void DrawFight (ScreenCoordinate piece1, ScreenCoordinate piece2)
		{


		}

		// Returns the grid co-ordinate, if a user has clicked on/near one.
		public bool HitPoint (ScreenCoordinate point, out GridCoordinate gridPoint)
		{
			int[] canvasLocations = new int[2];
			_gameMap.GetLocationOnScreen (canvasLocations);
			ScreenCoordinate pointRelativeToView = new ScreenCoordinate (point.X - canvasLocations [0], point.Y - canvasLocations [1]);
			for (int i = 0; i <= XDimension; i++)
				for (int j = 0; j <= YDimension; j++)
					if ((Math.Abs (ScreenLocations [i, j].X - pointRelativeToView.X) < _hitRadius) && (Math.Abs (ScreenLocations [i, j].Y - pointRelativeToView.Y) < _hitRadius)) {
						gridPoint = new GridCoordinate (i, j);
						return true;
					}
			
			gridPoint = null;
			return false;
		}

		// Returns the piece, if one was hit on the screen, otherwise, null
		public GamePiece HitPiece (ScreenCoordinate point)
		{
			int[] canvasLocations = new int[2];
			_gameMap.GetLocationOnScreen (canvasLocations);
			ScreenCoordinate pointRelativeToView = new ScreenCoordinate (point.X - canvasLocations [0], point.Y - canvasLocations [1]);
			
			foreach (GamePiece p in _gameMap.Pieces) {
				ScreenCoordinate piecePosition = p.PiecePosition.GetScreenCoordinate (ScreenLocations);
				if (Math.Abs (piecePosition.X - pointRelativeToView.X) < _hitRadius && Math.Abs (piecePosition.Y - pointRelativeToView.Y) < _hitRadius)
					return p;
			}
			return null;					
		}

		public void DrawMap (Canvas canvas)
		{
			Paint gridPen = new Paint ();
			gridPen.Color = Color.Green;
			gridPen.StrokeWidth = 2;
			gridPen.SetStyle (Paint.Style.Stroke);

			// TODO: Change this to use ScreenLocations instead of (stupidly) calculating the coordinates again.

			List<float> linesToDraw = new List<float> ();
			for (int x = 0; x <= XDimension; x++) {
				linesToDraw.Add (_borderPx + x * ((_screenXPx - (2 * _borderPx)) / XDimension));
				linesToDraw.Add (_borderPx);
				linesToDraw.Add (_borderPx + x * ((_screenXPx - (2 * _borderPx)) / XDimension));
				linesToDraw.Add (_borderPx + XDimension * ((_screenXPx - (2 * _borderPx)) / XDimension));
			}
			for (int y = 0; y <= YDimension; y++) {
				linesToDraw.Add (_borderPx);
				linesToDraw.Add (_borderPx + y * ((_screenYPx - (2 * _borderPx)) / YDimension));
				linesToDraw.Add (_borderPx + YDimension * ((_screenYPx - (2 * _borderPx)) / YDimension));
				linesToDraw.Add (_borderPx + y * ((_screenYPx - (2 * _borderPx)) / YDimension));
			}
			canvas.DrawLines (linesToDraw.ToArray (), gridPen);

			DrawPiecePaths (canvas);

			DrawCurrentPath (canvas);
		}

		public void DrawPiecePaths (Canvas canvas)
		{
			Paint pathPen = new Paint ();
			pathPen.Color = Color.Blue;
			pathPen.StrokeWidth = 4;
			pathPen.SetStyle (Paint.Style.Stroke);

			foreach (GamePiece piece in _gameMap.Pieces.Where(p => p.Path != null && p.Path.Count > 0)) {
				ScreenCoordinate pieceCoord = piece.PiecePosition.GetScreenCoordinate (ScreenLocations);
				ScreenCoordinate pathStart = ScreenLocations [piece.Path [0].X, piece.Path [0].Y];
				if (piece.PiecePosition.GetType () == typeof(MovingPosition)) {
					GridCoordinate mpTo = ((MovingPosition)piece.PiecePosition).To;
					ScreenCoordinate pieceTo = ScreenLocations [mpTo.X, mpTo.Y];
					canvas.DrawLines (new float[]{ pieceCoord.X, pieceCoord.Y, pieceTo.X, pieceTo.Y }, pathPen);
					canvas.DrawLines (new float[]{ pieceTo.X, pieceTo.Y, pathStart.X, pathStart.Y }, pathPen);
				} else {
					canvas.DrawLines (new float[]{ pieceCoord.X, pieceCoord.Y, pathStart.X, pathStart.Y }, pathPen);
				}
				for (int i = 0; i < piece.Path.Count - 1; i++) {
					ScreenCoordinate from = ScreenLocations [piece.Path [i].X, piece.Path [i].Y];
					ScreenCoordinate to = ScreenLocations [piece.Path [i + 1].X, piece.Path [i + 1].Y];
					canvas.DrawLines (new float[] { from.X, from.Y, to.X, to.Y }, pathPen);
				}
			}
		}

		public void DrawCurrentPath (Canvas canvas)
		{
			Paint pathPen = new Paint ();
			pathPen.Color = Color.MediumTurquoise;
			pathPen.StrokeWidth = 6;
			pathPen.SetStyle (Paint.Style.Stroke);

			if (_gameMap.ProvisionalPath != null && _gameMap.ProvisionalPath.Count > 0) {
				GamePiece gp = _gameMap.Pieces.FirstOrDefault (p => p.IsSelected);
				if (gp != null) {
					ScreenCoordinate pieceCoord = gp.PiecePosition.GetScreenCoordinate (ScreenLocations);
					ScreenCoordinate pathStart = ScreenLocations [_gameMap.ProvisionalPath [0].X, _gameMap.ProvisionalPath [0].Y];
					if (gp.PiecePosition.GetType () == typeof(MovingPosition)) {
						GridCoordinate mpTo = ((MovingPosition)gp.PiecePosition).To;
						ScreenCoordinate pieceTo = ScreenLocations [mpTo.X, mpTo.Y];
						canvas.DrawLines (new float[]{ pieceCoord.X, pieceCoord.Y, pieceTo.X, pieceTo.Y }, pathPen);
						canvas.DrawLines (new float[]{ pieceTo.X, pieceTo.Y, pathStart.X, pathStart.Y }, pathPen);
					} else {
						canvas.DrawLines (new float[]{ pieceCoord.X, pieceCoord.Y, pathStart.X, pathStart.Y }, pathPen);
					}
					for (int i = 0; i < _gameMap.ProvisionalPath.Count - 1; i++) {
						ScreenCoordinate from = ScreenLocations [_gameMap.ProvisionalPath [i].X, _gameMap.ProvisionalPath [i].Y];
						ScreenCoordinate to = ScreenLocations [_gameMap.ProvisionalPath [i + 1].X, _gameMap.ProvisionalPath [i + 1].Y];
						canvas.DrawLines (new float[] { from.X, from.Y, to.X, to.Y }, pathPen);
					}
				}
			}
		}
	}
}

