﻿namespace LifeCycleGame.GameStateMap
{
    public abstract class Position
	{
		public abstract ScreenCoordinate GetScreenCoordinate (ScreenCoordinate[,] gridToScreenArray);

		public abstract Position DeepClone ();
	}

	public class StaticPosition : Position
	{
		public readonly GridCoordinate At;

		public StaticPosition (GridCoordinate at)
		{
			At = at;	
		}

		public override ScreenCoordinate GetScreenCoordinate (ScreenCoordinate[,] gridToScreenArray)
		{
			return gridToScreenArray [At.X, At.Y];
		}

		public override Position DeepClone ()
		{
			return new StaticPosition ((GridCoordinate)this.At.DeepClone ());	
		}

		public override string ToString ()
		{
			return string.Format ("[StaticPosition: {0}]", At);
		}
	}

	public class MovingPosition : Position
	{
		public readonly GridCoordinate From;

		public readonly GridCoordinate To;

		public float PercentTo { get; set; }

		public MovingPosition (GridCoordinate from, GridCoordinate to, float percentTo)
		{
			From = from;
			To = to;
			PercentTo = percentTo;
		}

		public override ScreenCoordinate GetScreenCoordinate (ScreenCoordinate[,] gridToScreenArray)
		{
			ScreenCoordinate from = gridToScreenArray [From.X, From.Y];
			ScreenCoordinate to = gridToScreenArray [To.X, To.Y];
			return new ScreenCoordinate (
				((int)(from.X + (PercentTo / 100) * (to.X - from.X))),
				((int)(from.Y + (PercentTo / 100) * (to.Y - from.Y)))
			);
		}

		public override Position DeepClone ()
		{
			return new MovingPosition ((GridCoordinate)this.From.DeepClone (), (GridCoordinate)this.To.DeepClone (), this.PercentTo);
		}

		public override string ToString ()
		{
			return string.Format ("[MovingPosition (From {0}, To {1} PercentTo={2})]", From, To, PercentTo);
		}

	}
}

