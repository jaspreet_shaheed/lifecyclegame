﻿using System.Threading;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using LifeCycleGame.GameStateMap;

namespace LifeCycleGame
{
    [Activity (Label = "LifeCycleGame", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		GameEngine _gameEngine;
		//GestureDetector _gestureDetector;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			_gameEngine = new GameEngine (
				(LinearLayout)FindViewById (Resource.Id.game_board), 
				(LinearLayout)FindViewById (Resource.Id.unit_details), 
				Application.Context);
			Thread gameThread = new Thread (new ThreadStart (_gameEngine.Start));
		
			gameThread.Start ();
		}

		/// <param name="e">The touch screen event being processed.</param>
		/// <summary>
		/// At the moment, is only concerned with moving pieces around the board.
		/// Desired behaviour:
		/// 1) Select a piece if one is touched
		/// 2) As a player drags their finger along the board (starting on a piece) chart out (and display) the desired path for the piece.
		/// 3) The player should be able to cancel by sliding their finger back to the piece and letting go
		/// 4) When the user lifts their finger off the piece, start the piece moving along the path
		/// </summary>
		public override bool OnTouchEvent (MotionEvent e)
		{
			lock (_gameEngine.GameMap.GameStateLock) {
				
				if (e.Action == MotionEventActions.Down) {
					_gameEngine.GameMap.TouchOn (new ScreenCoordinate ((int)e.GetX (), (int)e.GetY ()));

				} else if (e.Action == MotionEventActions.Move) {
					_gameEngine.GameMap.AddToPath ((int)e.GetX (), (int)e.GetY ());

				} else if (e.Action == MotionEventActions.Up) {
					_gameEngine.GameMap.StartPieceMove ();				
				}
			}
			return false;
		}
	}
}