namespace LifeCycleGame.AI

open System
open System.Collections.Generic;
open System.Linq;
open LifeCycleGame.Shared;
open LifeCycleGame.Shared.Pieces;
open LifeCycleGame.GameStateMap;

type RandomWalk() = 
    interface LifeCycleGame.Shared.IStrategy with 
        member this.GetMove(gameState) = 
            let r = System.Random()
            let move = r.Next(4)
            let aiPieces =
              query {
                for piece in gameState.Pieces do
                where (piece :? AIPiece && piece.CanMove())
                select piece
            }
            if aiPieces = Seq.empty then
                (null, enum<Move> move)
            else 
                let pieceIndex = r.Next(aiPieces.Count())
                let chosenPiece = Seq.nth pieceIndex aiPieces  
                (chosenPiece, enum<Move> move)
