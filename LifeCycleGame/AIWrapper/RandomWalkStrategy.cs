using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LifeCycleGame.GameStateMap;
using LifeCycleGame.Shared;
using LifeCycleGame.Shared.Pieces;

namespace LifeCycleGame.AIWrapper
{
    public class RandomWalkStrategy : IStrategy
    {
        public Tuple<GamePiece, Move> GetMove(GameState gameState)
        {

            var r = new Random();
            var aiPieces = gameState.Pieces.Where(p => p is AIPiece && p.CanMove());
            if (aiPieces.Any())
                return new Tuple<GamePiece, Move>(aiPieces.ElementAt(r.Next(aiPieces.Count())), (Move)r.Next(4));
            else
                return new Tuple<GamePiece, Move>(null, Move.Down);
        }
    }
}