using System;
using System.Collections.Generic;

namespace LifeCycleGame.Shared.Pieces
{

    public abstract class RechargingPiece : GamePiece
	{
		public int CycleLength { get; set; }

		public int CyclePosition { get; set; }

		public virtual bool GettingStronger()
		{
			return CyclePosition * 2 < CycleLength;
		}

		public override void NextTurn()
		{
			base.NextTurn();
			CyclePosition++;
			if (CyclePosition == CycleLength)
				CyclePosition = 0;
		}

		/// <summary>
		/// Indicates how "strong" the piece should appear, depending upon it's position in the cycle
		/// </summary>
		/// <returns>float between 0 and 10 with 0 indicating weakest and 10 indicating strongest</returns>
		public override float ScaledStrength()
		{
			// Math.Abs (CycleLength/2-CyclePosition) First term will return a value between 0 and CycleLength / 2 depending upon how far from peak strength the unit is (0 being stronger)
			// Second term will normalise by multiplying by 10 and then dividing by CycleLength/2
			return GetStrengthAtCyclePosition(CyclePosition);
		}

		public float GetStrengthAtCyclePosition(int cyclePosition)
		{
			return Math.Abs(10 - (10 * 2 * Math.Abs((CycleLength / 2) - cyclePosition)) / CycleLength);
		}

		public override IEnumerable<float> GetStrengthAtIntervals(int numOfIntervals)
		{
			List<float> strengthAtIntervals = new List<float>();
			for (int i = 0; i < numOfIntervals; i++)
			{
				int cyclePos = (int)i * (CycleLength / (numOfIntervals - 1));
				strengthAtIntervals.Add(GetStrengthAtCyclePosition(cyclePos));
			}
			return strengthAtIntervals;
		}
	}

}