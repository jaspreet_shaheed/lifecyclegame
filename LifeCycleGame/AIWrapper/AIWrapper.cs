﻿using LifeCycleGame.GameStateMap;
using LifeCycleGame.Shared;
using System;
using System.Threading;

namespace LifeCycleGame.AIWrapper
{
    public class AIWrapper
	{
		protected readonly GameMap _gameMap;
		protected int _sleepInterval = 3000;    // This should ensure the AI doesn't move pieces about too quickly.
        protected readonly IStrategy Strategy;

		public AIWrapper(GameMap gameMap, IStrategy strategy)
		{
			_gameMap = gameMap;
            Strategy = strategy;
		}

		public void Start ()
		{
			while (true) {
				GameState gameState;

				// Get a copy of the GameState
				lock (_gameMap.GameStateLock)
					gameState = _gameMap.GetGameState (); 
				
				// Figure out what move to make
				var move = Strategy.GetMove (gameState);	

				// If there was a possible/desirable move, then execute it
				if (move.Item1 != null)
					lock (_gameMap.GameStateLock)
						_gameMap.ExecuteMove (move.Item1.ID, move.Item2);
				
				// Sleep
				Thread.Sleep (_sleepInterval);
			}
		}
	}
}

