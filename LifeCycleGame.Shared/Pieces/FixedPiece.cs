using System.Collections.Generic;

namespace LifeCycleGame.Shared.Pieces
{

    public abstract class FixedPiece : GamePiece
	{
		public int MaxHitPoints { get; set; }

		public int RemainingHitPoints { get; set; }

		public override float ScaledStrength()
		{
			return GetStrengthAtHitPoint(RemainingHitPoints);
		}

		public float GetStrengthAtHitPoint(int hitPoint)
		{
			return 10 * ((float)hitPoint / (float)MaxHitPoints);
		}

		public override IEnumerable<float> GetStrengthAtIntervals(int numOfIntervals)
		{
			List<float> strengthAtIntervals = new List<float>();
			for (int i = 0; i < numOfIntervals; i++)
			{
				int cyclePos = (int)i * MaxHitPoints / (numOfIntervals - 1);
				strengthAtIntervals.Add(GetStrengthAtHitPoint(cyclePos));
			}
			return strengthAtIntervals;
		}
	}
}