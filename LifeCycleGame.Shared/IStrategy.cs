using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LifeCycleGame.Shared.Pieces;
using LifeCycleGame.GameStateMap;

namespace LifeCycleGame.Shared
{
    public interface IStrategy
    {
        Tuple<GamePiece, Move> GetMove(GameState gameState);
    }
}