﻿using System.Linq;
using System.Threading;
using Android.Views;
using Android.App;
using Android.Content;
using Android.Widget;
using LifeCycleGame.AIWrapper;
using LifeCycleGame.GameStateMap;
using LifeCycleGame.Shared.Pieces;
using LifeCycleGame.GameInfo;
using LifeCycleGame.Shared;

namespace LifeCycleGame
{
    public class GameEngine : Activity
	{
		private int _sleepBetweenTurnsInMs = 10;

		public GameMap GameMap { get; set; }

		public UnitInfo UnitInfo { get; set; }

		private AIWrapper.AIWrapper _AIPlayer;

		public GameEngine (LinearLayout gameBoard, LinearLayout unitInfo, Context c)
		{
			UnitInfo = new UnitInfo (c);		
			GameMap = new GameMap (c, UnitInfo);		
			unitInfo.AddView (UnitInfo, new ViewGroup.LayoutParams (ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent));
			gameBoard.AddView (GameMap, new ViewGroup.LayoutParams (ViewGroup.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent));
			_AIPlayer = new AIWrapper.AIWrapper(GameMap, new RandomWalkStrategy());
		}

		public void Start ()
		{
			Thread aiThread = new Thread (new ThreadStart (_AIPlayer.Start));
			aiThread.Start ();

			bool done = false;
			while (!done) {

				lock (GameMap.GameStateLock) {
					foreach (var piece in GameMap.Pieces)
						piece.NextTurn ();

					foreach (var fight in GameMap.Fights)
						ProcessFight (fight.Item1, fight.Item2);
				
					RemoveDeadPieces ();

					RunOnUiThread (() => {
						UnitInfo.Invalidate ();
						GameMap.Invalidate ();
					});
				}
				Thread.Sleep (_sleepBetweenTurnsInMs);
			}
		}

		private void ProcessFight (GamePiece piece1, GamePiece piece2)
		{
			if (!piece1.IsDead () && !piece2.IsDead ()) {
				GameMap.FightOneRound (piece1, piece2);
				GameMap.MapDrawer.DrawFight (piece1.PiecePosition.GetScreenCoordinate (GameMap.MapDrawer.ScreenLocations), piece2.PiecePosition.GetScreenCoordinate (GameMap.MapDrawer.ScreenLocations));
			}
		}

		private void RemoveDeadPieces ()
		{
            var deadPieces = GameMap.Pieces.Where(p => p.IsDead()).ToList();
            deadPieces.ForEach (GameMap.StopFighting);
            deadPieces.ForEach( UnitInfo.PieceDiedSoPotentiallyRemove);
            GameMap.Pieces.RemoveAll (p => p.IsDead());
		}
	}
}

