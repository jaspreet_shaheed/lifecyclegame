﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using LifeCycleGame.Shared.Pieces;

namespace LifeCycleGame.GameStateMap.Drawers
{
    public class PieceDrawer
    {
        readonly int _moveRechargeRadius;
        readonly int _selectChevronLength;
        private MapDrawer _mapDrawer;
        private GameMap _gameMap;

        public PieceDrawer(Context context, MapDrawer mapDrawer, int xDimension, int yDimension, GameMap gameMap)
        {
            _mapDrawer = mapDrawer;
            _gameMap = gameMap;
            PieceRadius = (int)Math.Min((gameMap.Resources.DisplayMetrics.WidthPixels / (xDimension * 4)), (gameMap.Resources.DisplayMetrics.HeightPixels / (yDimension * 4)));
            _moveRechargeRadius = (int)PieceRadius / 3;
            _selectChevronLength = (int)(PieceRadius / 3);
        }

        public int PieceRadius { get; set; }

        private void DrawAIPiece(Canvas canvas, AIPiece gp)
        {
            ScreenCoordinate piecelocation = gp.PiecePosition.GetScreenCoordinate(_mapDrawer.ScreenLocations);
            Paint piecePen = new Paint();
            piecePen.SetStyle(Paint.Style.Fill);

            piecePen.Color = new Color(ColorIntensityFromStrength(gp.ScaledStrength()), 0, 0);
            canvas.DrawCircle(piecelocation.X, piecelocation.Y, PieceRadius, piecePen);
        }

        private int ColorIntensityFromStrength(float zeroToTen)
        {
            int intensity = (int)(65 + (19 * zeroToTen));   // From dark to light blue (65 -> FF in blue range)
            if (intensity > 255)
                return 255;
            else if (intensity < 0)
                return 0;
            return intensity;
        }


        private void DrawPlayerPiece(Canvas canvas, PlayerPiece gp)
        {
            ScreenCoordinate pieceLocation = gp.PiecePosition.GetScreenCoordinate(_mapDrawer.ScreenLocations);
            Paint piecePen = new Paint();
            piecePen.SetStyle(Paint.Style.Fill);

            piecePen.Color = new Color(0, 0, ColorIntensityFromStrength(gp.ScaledStrength()));
            canvas.DrawCircle(pieceLocation.X, pieceLocation.Y, PieceRadius, piecePen);

            // Draw up or down arrows indicating whether the unit is getting stronger or weaker.
            Paint arrow = new Paint() { Color = Color.Green };
            arrow.StrokeWidth = 4;
            arrow.SetStyle(Paint.Style.Fill);
            if (gp.GettingStronger())
            {
                canvas.DrawLines(new float[] {
                    pieceLocation.X + PieceRadius - 6, pieceLocation.Y - PieceRadius + 6,
                    pieceLocation.X + PieceRadius - 3, pieceLocation.Y - PieceRadius + 10,
                    pieceLocation.X + PieceRadius - 9, pieceLocation.Y - PieceRadius + 10,
                    pieceLocation.X + PieceRadius - 6, pieceLocation.Y - PieceRadius + 6
                }, arrow);
            }
            else
            {
                canvas.DrawLines(new float[] {
                    pieceLocation.X + PieceRadius - 6, pieceLocation.Y + PieceRadius - 6,
                    pieceLocation.X + PieceRadius - 3, pieceLocation.Y + PieceRadius - 10,
                    pieceLocation.X + PieceRadius - 9, pieceLocation.Y + PieceRadius - 10,
                    pieceLocation.X + PieceRadius - 6, pieceLocation.Y + PieceRadius - 6
                }, arrow);
            }



        }

        public void DrawPieces(Canvas canvas)
        {
            Paint pieceMoveRechargeIndicator = new Paint() { Color = Color.GreenYellow };
            pieceMoveRechargeIndicator.SetStyle(Paint.Style.Fill);

            foreach (GamePiece gp in _gameMap.Pieces)
            {

                if (gp.GetType() == typeof(PlayerPiece))
                    DrawPlayerPiece(canvas, (PlayerPiece)gp);
                else if (gp.GetType() == typeof(AIPiece))
                    DrawAIPiece(canvas, (AIPiece)gp);

                ScreenCoordinate piecelocation = gp.PiecePosition.GetScreenCoordinate(_mapDrawer.ScreenLocations);
                RectF moveOval = new RectF(piecelocation.X - _moveRechargeRadius,
                                     piecelocation.Y - _moveRechargeRadius,
                                     piecelocation.X + _moveRechargeRadius,
                                     piecelocation.Y + _moveRechargeRadius);

                canvas.DrawArc(
                    moveOval,
                    0,
                    gp.MoveRechargePosition * (360 / gp.MoveRechargeLength),
                    true,
                    pieceMoveRechargeIndicator);


                if (gp.IsSelected)
                {       // If the piece is selected, draw some chevrons around it to indicate it is selected
                    List<float> chevronLines = new List<float>() {
                        piecelocation.X - PieceRadius,
                        piecelocation.Y - PieceRadius + _selectChevronLength,
                        piecelocation.X - PieceRadius,
                        piecelocation.Y - PieceRadius,	// Line 1
						piecelocation.X - PieceRadius,
                        piecelocation.Y - PieceRadius,
                        piecelocation.X - PieceRadius + _selectChevronLength,
                        piecelocation.Y - PieceRadius,	// Line 2
						piecelocation.X + PieceRadius,
                        piecelocation.Y - PieceRadius + _selectChevronLength,
                        piecelocation.X + PieceRadius,
                        piecelocation.Y - PieceRadius,	// Line 3
						piecelocation.X + PieceRadius,
                        piecelocation.Y - PieceRadius,
                        piecelocation.X + PieceRadius - _selectChevronLength,
                        piecelocation.Y - PieceRadius,	// Line 4
						piecelocation.X + PieceRadius,
                        piecelocation.Y + PieceRadius - _selectChevronLength,
                        piecelocation.X + PieceRadius,
                        piecelocation.Y + PieceRadius,	// Line 5
						piecelocation.X + PieceRadius,
                        piecelocation.Y + PieceRadius,
                        piecelocation.X + PieceRadius - _selectChevronLength,
                        piecelocation.Y + PieceRadius,	// Line 6
						piecelocation.X - PieceRadius,
                        piecelocation.Y + PieceRadius - _selectChevronLength,
                        piecelocation.X - PieceRadius,
                        piecelocation.Y + PieceRadius,	// Line 7
						piecelocation.X - PieceRadius,
                        piecelocation.Y + PieceRadius,
                        piecelocation.X - PieceRadius + _selectChevronLength,
                        piecelocation.Y + PieceRadius,	// Line 8
					};
                    Paint chevronPen = new Paint() { Color = Color.GreenYellow, StrokeWidth = 3 };
                    chevronPen.SetStyle(Paint.Style.Stroke);
                    canvas.DrawLines(chevronLines.ToArray(), chevronPen);
                }
            }
        }
    }
}

