﻿namespace LifeCycleGame.GameStateMap
{
    public abstract class Coordinate
	{
		public readonly int X, Y;

		public Coordinate (int x, int y)
		{
			this.X = x;
			this.Y = y;
		}

		public static bool operator == (Coordinate c1, Coordinate c2)
		{
			return c1.Equals (c2);
		}

		public static bool operator != (Coordinate c1, Coordinate c2)
		{
			return !c1.Equals (c2);
		}

		public override bool Equals (object ob)
		{
			if (ob is Coordinate) {
				Coordinate c = (Coordinate)ob;
				return X == c.X && Y == c.Y;
			} else {
				return false;
			}
		}

		public override int GetHashCode ()
		{
			return X.GetHashCode () ^ Y.GetHashCode ();
		}

		public abstract Coordinate DeepClone ();

		public override string ToString ()
		{
			return string.Format ("({0},{1})", X, Y);
		}

	}

	public class GridCoordinate : Coordinate
	{
		public GridCoordinate (int x, int y) : base (x, y)
		{
		}

		public override Coordinate DeepClone ()
		{
			return new GridCoordinate (this.X, this.Y);

		}
	}

	public class ScreenCoordinate : Coordinate
	{
		public ScreenCoordinate (int x, int y) : base (x, y)
		{
		}

		public override Coordinate DeepClone ()
		{
			return new ScreenCoordinate (this.X, this.Y);
		}
	}
}