﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Graphics;
using LifeCycleGame.Shared.Pieces;
using LifeCycleGame.Shared;
using LifeCycleGame.GameStateMap.Drawers;
using LifeCycleGame.GameInfo;

namespace LifeCycleGame.GameStateMap
{
    public class GameMap : View
	{
		public GameState GameState { get; set; }

		public List<GamePiece> Pieces
		{
			get { return GameState.Pieces; }
			set { GameState.Pieces = value; }
		}

		public MapDrawer MapDrawer { get; set; }

		public PieceDrawer PieceDrawer { get; set; }

		public UnitInfo UnitInfo { get; set; }

		public List<Tuple<GamePiece, GamePiece>> Fights { get; set; }

		public GameMap(Context context, UnitInfo unitInfo) :
			base(context)
		{
			Initialize(context, unitInfo);
		}

		public GameMap(Context context, UnitInfo unitInfo, IAttributeSet attrs) :
			base(context, attrs)
		{
			Initialize(context, unitInfo);
		}

		public GameMap(Context context, UnitInfo unitInfo, IAttributeSet attrs, int defStyle) :
			base(context, attrs, defStyle)
		{
			Initialize(context, unitInfo);
		}

		public Object GameStateLock = new Object();

		// Note this is number of squares- but units move along the interesections so there will be _xTiles + 1 possible X positions
		private int XDimension
		{
			get { return GameState.XDimension; }
			set { GameState.XDimension = value; }
		}

		private int YDimension
		{
			get { return GameState.YDimension; }
			set { GameState.YDimension = value; }
		}

		public GameState GetGameState()
		{
			return GameState.DeepClone();
		}

		public void Initialize(Context context, UnitInfo unitInfo)
		{
			GameState = new GameState();
			XDimension = 7;
			YDimension = 7;
			MapDrawer = new MapDrawer(context, XDimension, YDimension, this);
			PieceDrawer = new PieceDrawer(context, MapDrawer, XDimension, YDimension, this);
			Fights = new List<Tuple<GamePiece, GamePiece>>();
			Pieces = new List<GamePiece>();
			Pieces.Add(new PlayerPiece(1, 1, 1, this));
			Pieces.Add(new PlayerPiece(2, 2, 2, this));
            Pieces.Add(new PlayerPiece(3, 3, 3, this));
            Pieces.Add(new AIPiece(4, 4, 4, this));
			Pieces.Add(new AIPiece(5, 5, 5, this));
            Pieces.Add(new AIPiece(6, 6, 6, this));
            UnitInfo = unitInfo;
		}

		public List<GridCoordinate> ProvisionalPath = new List<GridCoordinate>();

		public bool AdjacentTo(GridCoordinate g1, GridCoordinate g2)
		{
			return (g1.X == g2.X && ((g1.Y == g2.Y + 1) || (g1.Y == g2.Y - 1))) || (g1.Y == g2.Y && ((g1.X == g2.X + 1) || (g1.X == g2.X - 1)));
		}

		public bool MovingAPiece { get; set; }

		// If we touch a piece, set it's selected as true and if it's a playerpiece, record that we're touching it
		public void TouchOn(ScreenCoordinate point)
		{
			MovingAPiece = false;
			GamePiece pieceTouched = MapDrawer.HitPiece(point);
			if (pieceTouched != null)
			{
				Pieces.ForEach(p => p.IsSelected = false);  // Unset any selections
				pieceTouched.IsSelected = true;
				UnitInfo.SetPieceToDisplay(pieceTouched);
				if (pieceTouched.GetType() == typeof(PlayerPiece))
				{
					ProvisionalPath = new List<GridCoordinate>();
					MovingAPiece = true;
				}
			}
		}

		// 1. If we've gone back to the penultimate point, then remove the last element in the path
		// 2. If we've hit a point Up, Down, Left or Right of the last element in the path, then add the point to the path
		public void AddToPath(int x, int y)
		{
			GridCoordinate GC;
			GamePiece gp = Pieces.FirstOrDefault(p => p.IsSelected);
			if (MovingAPiece && MapDrawer.HitPoint(new ScreenCoordinate(x, y), out GC))
			{
				if (ProvisionalPath != null && ProvisionalPath.Count > 0)
				{
					GridCoordinate lastGC = ProvisionalPath.Last();
					if (ProvisionalPath.Count > 1)
					{
						GridCoordinate beforeLastGC = ProvisionalPath[ProvisionalPath.Count - 2];
						if (beforeLastGC == GC)
							ProvisionalPath.Remove(lastGC);
						else if (AdjacentTo(GC, lastGC))
							ProvisionalPath.Add(GC);
					}
					else if (ProvisionalPath.Count == 1 && gp != null && gp.PiecePosition.GetType() == typeof(StaticPosition) &&
							 ((StaticPosition)gp.PiecePosition).At == GC)
					{
						ProvisionalPath = new List<GridCoordinate>();   // Clear the path					
					}
					else if (AdjacentTo(GC, lastGC))
					{
						ProvisionalPath.Add(GC);
					}
				}
				else if (ProvisionalPath == null || ProvisionalPath.Count == 0)
				{
					if (gp != null && IsReachableFrom(gp.PiecePosition, GC))
					{
						ProvisionalPath = new List<GridCoordinate> { GC };
					}
				}
			}
		}

		public void StartPieceMove()
		{
			GamePiece pieceToMove = Pieces.FirstOrDefault(p => p.IsSelected);
			if (pieceToMove != null && pieceToMove.GetType() == typeof(PlayerPiece) && ProvisionalPath != null && ProvisionalPath.Count > 0)
			{
				PlayerPiece playerPiece = (PlayerPiece)pieceToMove;
				playerPiece.StartMoveTo(ProvisionalPath);
			}
			ProvisionalPath = new List<GridCoordinate>();
			MovingAPiece = false;
		}

		private bool WithinGridBounds(StaticPosition c)
		{
			return c.At.X <= MapDrawer.XDimension && c.At.X >= 0 && c.At.Y <= MapDrawer.YDimension && c.At.Y >= 0;
		}

		public GamePiece BlockedBy(GamePiece gp, ScreenCoordinate c)
		{
			return Pieces.FirstOrDefault(p => p != gp && Math.Sqrt(Math.Pow(Math.Abs(p.PiecePosition.GetScreenCoordinate(MapDrawer.ScreenLocations).X - c.X), 2) + Math.Pow(Math.Abs(p.PiecePosition.GetScreenCoordinate(MapDrawer.ScreenLocations).Y - c.Y), 2)) < 2 * PieceDrawer.PieceRadius);
		}

		public bool IsFighting(GamePiece piece)
		{
			return Fights.Any(f => f.Item1 == piece || f.Item2 == piece);
		}

		public bool IsFighting(GamePiece piece1, GamePiece piece2)
		{
			return Fights.Any(f => (f.Item1 == piece1 && f.Item2 == piece2) || (f.Item1 == piece2 && f.Item2 == piece1));
		}

		public void StartFighting(GamePiece piece1, GamePiece piece2)
		{
			Fights.Add(new Tuple<GamePiece, GamePiece>(piece1, piece2));
		}

		public void StopFighting(GamePiece piece)
		{
			Fights.RemoveAll(f => f.Item1 == piece || f.Item2 == piece);
		}

		public void FightOneRound(GamePiece piece1, GamePiece piece2)
		{
			Random r = new Random();
			int outcome = r.Next(0, piece1.Strength + piece2.Strength);
			if (outcome < piece1.Strength)
				piece2.LoseFight();
			else
				piece1.LoseFight();
		}

		public void ExecuteMove(int pieceID, Move move)
		{
			GamePiece toMove = Pieces.FirstOrDefault(p => p.ID == pieceID);
			if (toMove != null)
				ExecuteMove(toMove, move);
		}

		public void ExecuteMove(GamePiece gp, GridCoordinate to)
		{
			if (gp.PiecePosition.GetType() == typeof(StaticPosition))
			{
				GridCoordinate at = ((StaticPosition)gp.PiecePosition).At;
				StaticPosition moveTo = new StaticPosition(to);
				if (at == to)
					throw new Exception("At and To the same");

				if (WithinGridBounds(moveTo) && gp.CanMove())
				{
					gp.PiecePosition = new MovingPosition(at, moveTo.At, 0);
					gp.MoveRechargePosition = 0;
				}
			}
			else if (gp.PiecePosition.GetType() == typeof(MovingPosition))
			{
				// If we're currently moving, the only change allowed is one opposite to the one we're making (so we & the AI can back out of deadlocks!)
				MovingPosition mp = (MovingPosition)gp.PiecePosition;
				if (mp.From == to)
				{
					gp.PiecePosition = new MovingPosition(mp.To, mp.From, 100 - mp.PercentTo);
					gp.MoveRechargePosition = 0;
				}
			}
		}

		public void ExecuteMove(GamePiece gp, Move move)
		{
			if (gp.PiecePosition.GetType() == typeof(StaticPosition))
			{
				GridCoordinate at = ((StaticPosition)gp.PiecePosition).At;
				GridCoordinate moveTo;
				if (move == Move.Down)
				{
					moveTo = new GridCoordinate(at.X, at.Y + 1);
				}
				else if (move == Move.Up)
				{
					moveTo = new GridCoordinate(at.X, at.Y - 1);
				}
				else if (move == Move.Left)
				{
					moveTo = new GridCoordinate(at.X - 1, at.Y);
				}
				else {
					moveTo = new GridCoordinate(at.X + 1, at.Y);
				}
				ExecuteMove(gp, moveTo);
			}
			else if (gp.PiecePosition.GetType() == typeof(MovingPosition))
			{
				// If we're currently moving, the only change allowed is one opposite to the one we're making (so we & the AI can back out of deadlocks!)
				MovingPosition mp = (MovingPosition)gp.PiecePosition;
				ExecuteMove(gp, mp.From);
			}
		}

		public bool IsReachableFrom(Position p, GridCoordinate to)
		{
			if (p.GetType() == typeof(StaticPosition))
			{
				StaticPosition sp = (StaticPosition)p;
				return AdjacentTo(sp.At, to);
			}
			else {
				MovingPosition mp = (MovingPosition)p;
				return (mp.From == to || mp.To == to);
			}
		}

		private bool OppositeMove(Move m1, Move m2)
		{
			return ((m1 == Move.Up && m2 == Move.Down) || (m1 == Move.Down && m2 == Move.Up) || (m1 == Move.Left && m2 == Move.Right) || (m1 == Move.Right && m2 == Move.Left));
		}

		private Move GetMoveFromMovingPosition(MovingPosition mp)
		{
			if (mp.To.X - mp.From.X == 1)
				return Move.Right;
			else if (mp.From.X - mp.To.X == 1)
				return Move.Left;
			else if (mp.To.Y - mp.From.Y == 1)
				return Move.Down;
			else
				return Move.Up;
		}

		public Move GetMoveFromGridCoordinates(GridCoordinate g1, GridCoordinate g2)
		{
			if (g1.X == g2.X && g2.Y > g1.Y)
				return Move.Down;
			else if (g1.X == g2.X && g2.Y < g1.Y)
				return Move.Up;
			else if (g1.Y == g2.Y && g2.X < g1.X)
				return Move.Left;
			else if (g1.Y == g2.Y && g2.X > g1.X)
				return Move.Right;

			throw new Exception(string.Format("Gridcoordinates don't appear to be adjacent: {0}, {1}", g1, g2));
		}

		protected override void OnDraw(Canvas canvas)
		{
			lock (GameStateLock)
			{
				MapDrawer.DrawMap(canvas);
				PieceDrawer.DrawPieces(canvas);
			}
		}
	}

	public enum Move
	{
		Left = -0,
		Right = 1,
		Up = 2,
		Down = 3
	}
}

