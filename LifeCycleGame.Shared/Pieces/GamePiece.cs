﻿using LifeCycleGame.GameStateMap;
using System;
using System.Collections.Generic;

namespace LifeCycleGame.Shared.Pieces
{
    public abstract class GamePiece
	{
		public int ID;

		protected int _movePercentPerCycle = 1;

		protected object _pieceLock = new object();

		public Position PiecePosition { get; set; }

		public int Strength { get; set; }

		// Ideally should be divisor of 360
		public int MoveRechargeLength { get; set; }

		public int MoveRechargePosition { get; set; }

		public abstract bool CanMove();

		public abstract bool IsDead();

		public bool IsSelected { get; set; }

		public abstract float ScaledStrength();

		protected GameMap GameMap { get; set; }

		public abstract void LoseFight();

		public abstract GamePiece DeepClone();

		public bool CanFight(GamePiece gp)
		{
			return !Object.ReferenceEquals(gp.GetType(), this.GetType());
		}

		public abstract IEnumerable<float> GetStrengthAtIntervals(int numIntervals);

		public List<GridCoordinate> Path { get; set; }

		public virtual void NextTurn()
		{
			if (MoveRechargePosition != MoveRechargeLength)
				MoveRechargePosition++;

			// If we're in the process of moving, determine if there is anything blocking us. If not, update our position
			if (PiecePosition.GetType() == typeof(MovingPosition))
			{
				MovingPosition p = (MovingPosition)PiecePosition;
				Position provisionalMove;
				if (p.PercentTo + _movePercentPerCycle >= 100)
					provisionalMove = new StaticPosition(p.To);
				else
					provisionalMove = new MovingPosition(p.From, p.To, p.PercentTo + _movePercentPerCycle);

				GamePiece blockingPiece = GameMap.BlockedBy(this, provisionalMove.GetScreenCoordinate(GameMap.MapDrawer.ScreenLocations));
				// If there is nothing block us, we update our position, otherwise if we're being blocked by an opposing piece, fight it!
				if (blockingPiece == null)
				{
					PiecePosition = provisionalMove;
					GameMap.StopFighting(this);
				}
				else if (!GameMap.IsFighting(blockingPiece) && CanFight(blockingPiece))
				{
					GameMap.StartFighting(this, blockingPiece);
				}
			}
		}

		public virtual string Title()
		{
			return $"{this.GetType().ToString()} ID: {this.ID}";
		}

		public virtual string Description()
		{
			return $"Strength: {this.Strength}";
		}

		public virtual string Events()
		{
			return "N/A";
		}
	}
}