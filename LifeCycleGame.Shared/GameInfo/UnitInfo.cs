﻿using Android.Views;
using Android.Content;
using Android.Util;
using Android.Graphics;
using LifeCycleGame.Shared.Pieces;

namespace LifeCycleGame.GameInfo
{
    public class UnitInfo : View
	{
		public UnitInfo(Context context) :
			base(context)
		{
			Initialize(context);
		}

		public UnitInfo(Context context, IAttributeSet attrs) :
			base(context, attrs)
		{
			Initialize(context);
		}

		public UnitInfo(Context context, IAttributeSet attrs, int defStyle) :
			base(context, attrs, defStyle)
		{
			Initialize(context);
		}

		public void Initialize(Context context)
		{
		}

		private GamePiece _pieceToDisplay;

		public void SetPieceToDisplay(GamePiece pieceToDisplay)
		{
			_pieceToDisplay = pieceToDisplay;
		}

        public void PieceDiedSoPotentiallyRemove(GamePiece pieceThatDied)
        {
            if (_pieceToDisplay.ID == pieceThatDied.ID)
                _pieceToDisplay = null;
        }

		protected override void OnDraw(Canvas canvas)
		{
			Paint textPen = new Paint { Color = Color.Green };
			int textSize = 25;
			int offSet = 30;
			int padding = 5;
			textPen.TextSize = textSize;
			textPen.SetTypeface(Typeface.Monospace);
			if (_pieceToDisplay != null)
			{
				canvas.DrawText($"{_pieceToDisplay.Title()}", offSet, offSet, textPen);
				canvas.DrawText($"{_pieceToDisplay.Description()}", offSet, offSet + textSize + padding, textPen);
			}
		}
	}
}

