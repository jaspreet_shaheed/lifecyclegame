﻿using System;
using System.Linq;
using System.Collections.Generic;
using LifeCycleGame.GameStateMap;

namespace LifeCycleGame.Shared.Pieces
{
	public class PlayerPiece : RechargingPiece
	{
		public PlayerPiece(int ID, int x, int y, GameMap gameMap)
		{
			CycleLength = 5000;
			CyclePosition = 0;
			PiecePosition = new StaticPosition(new GridCoordinate(x, y));
			Strength = 10;
			MoveRechargeLength = 180;
			MoveRechargePosition = 0;
			this.ID = ID;
			GameMap = gameMap;
		}

		public PlayerPiece()
		{
		}

		public override GamePiece DeepClone()
		{
			return new PlayerPiece()
			{
				ID = this.ID,
				CycleLength = this.CycleLength,
				CyclePosition = this.CyclePosition,
				PiecePosition = this.PiecePosition.DeepClone(),
				Strength = this.Strength,
				MoveRechargeLength = this.MoveRechargeLength,
				MoveRechargePosition = this.MoveRechargePosition,
				GameMap = null // No need to copy this
			};
		}

		public override bool IsDead()
		{
			return CyclePosition < 0 || CyclePosition >= CycleLength;
		}

		public override bool CanMove()
		{
			return (MoveRechargeLength == MoveRechargePosition);
		}

		// Losing a fight takes the unit away from it's strongest state (when CyclePosition = CycleLength/2)
		public override void LoseFight()
		{
			if (CyclePosition < CycleLength / 2)
				CyclePosition = CyclePosition - 10;
			else
				CyclePosition = CyclePosition + 10;
		}

		public override void NextTurn()
		{
			base.NextTurn();
			if (_blockedAndChangedDirection)
			{
				_blockedAndChangedDirection = false;
			}
			else if (CanMove() && Path != null && Path.Count > 0 && PiecePosition.GetType() == typeof(StaticPosition))
			{
				GridCoordinate toMoveTo = Path.First();
				Path.Remove(toMoveTo);
				if (IsSelected && GameMap.MovingAPiece)
				{
					if (GameMap.ProvisionalPath != null && GameMap.ProvisionalPath.Count > 0 && GameMap.ProvisionalPath.First() == ((StaticPosition)PiecePosition).At)
					{
						// We've passed a point we were supposed to go
						GridCoordinate pf = GameMap.ProvisionalPath.First();
						GameMap.ProvisionalPath.Remove(pf);
					}
					else {
						// We're moving 
						GameMap.ProvisionalPath.Insert(0, ((StaticPosition)PiecePosition).At);
					}
				}
				if (((StaticPosition)PiecePosition).At != toMoveTo)
				{
					GameMap.ExecuteMove(this, toMoveTo);
				}
			}
		}

		bool _blockedAndChangedDirection;
		public void StartMoveTo(List<GridCoordinate> path)
		{
			Path = path;
			if (CanMove() && PiecePosition.GetType() == typeof(MovingPosition))
			{
				GridCoordinate toMoveTo = Path.First();
				GameMap.ExecuteMove(this, toMoveTo);
				Path.Remove(toMoveTo);
			}
			else if (PiecePosition.GetType() == typeof(MovingPosition))
			{
				_blockedAndChangedDirection = true;
			}
		}

		public override string Description()
		{
			return $"Recharging Piece. Strength: {Math.Round(GetStrengthAtCyclePosition(CyclePosition), 2)}/{Strength}, Cycle Position: {CyclePosition}/{CycleLength}";
		}
	}
}