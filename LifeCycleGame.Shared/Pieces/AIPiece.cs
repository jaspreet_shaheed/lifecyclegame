﻿using LifeCycleGame.GameStateMap;

namespace LifeCycleGame.Shared.Pieces
{
    public class AIPiece : FixedPiece
	{
		public override bool CanMove()
		{
			return (MoveRechargeLength == MoveRechargePosition);
		}

		public override bool IsDead()
		{
			return RemainingHitPoints <= 0;
		}

		public override void LoseFight()
		{
			RemainingHitPoints = RemainingHitPoints - 10;
		}

		public AIPiece()
		{
		}

		public const int WaitCycles = 180;
		public int CyclesWaited { get; set; }

		public AIPiece(int ID, int x, int y, GameMap gameMap)
		{
			PiecePosition = new StaticPosition(new GridCoordinate(x, y));
			Strength = 10;
			MoveRechargeLength = 180;
			MoveRechargePosition = 0;
			MaxHitPoints = 2500;
			RemainingHitPoints = 2500;
			GameMap = gameMap;
			this.ID = ID;
		}

		public override GamePiece DeepClone()
		{
			return new AIPiece()
			{
				ID = this.ID,
				PiecePosition = this.PiecePosition.DeepClone(),
				Strength = this.Strength,
				MoveRechargeLength = this.MoveRechargeLength,
				MoveRechargePosition = this.MoveRechargePosition,
				MaxHitPoints = this.MaxHitPoints,
				RemainingHitPoints = this.RemainingHitPoints,
				GameMap = null // Should not need to set this. We should not need access the GameStateMap via cloned state
			};
		}

		public override string Description()
		{
			return $"Fixed Strength Piece. Strength {Strength}, HitPoints: {RemainingHitPoints}/{MaxHitPoints}";
		}
	}
}