﻿This is an idea for an android game I'm working on. I've started with only a vague notion of the gameplay I'd like to achieve. I'd like this to be an RTS game where the strength of the units varies sinusoidally so that 
at some points they're strong and at some points they're weak. I think this could lead to flowing strategic gameplay with players sometimes needing to run from the AI player. It is closer to being started than being 
finished and I haven't given a great deal of forethought to the architecture but am making it up as I go along so I can't guarantee that you'll find it playable or even compilable/in a consistent state.

Thanks,

Jaspreet